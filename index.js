// Webpack loader that
//   * converts soy files to js
//   * scans for its dependencies on other soy files
//   * appends an AMD define() to the end of the file which includes it's dependencies and returns to the template,
//     which is a JS global.
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var parsers = require("atlassian-wrm").parsers;
var _ = require("lodash");
var fsPath = require("path");
var finder = require("fs-finder");
var childProcess = require("child_process");
var fs = require("fs");
var temp = require("temp").track();
var soyDependencyMap = {};
var BUILT_IN_JAR = __dirname + "/atlassian-soy-cli-support-4.4.1-jar-with-dependencies.jar";
var tempDirectory = void 0;

function formatOptsForExec(obj) {
    return _.values(_.mapValues(obj, function (value, name) {
        var val = _.isArray(value) ? value.split(" ") : value;
        return "--" + name + " " + val;
    })).join(" ");
}

function compileSoy(_ref, dir, file) {
    var cwd = _ref.cwd,
        jar = _ref.jar,
        functions = _ref.functions;

    var options = {
        type: "js",
        glob: file ? file : "**/*.soy",
        basedir: dir,
        outdir: tempDirectory,
        functions: functions
    };
    var execParams = formatOptsForExec(options);
    var command = "java -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN -jar " + jar + " " + execParams + " --use-ajs-context-path --use-ajs-i18n";
    childProcess.execSync(command, { cwd: cwd });
}

function parseSoyFile(base, module, file) {
    try {
        var _ret = function () {
            var descriptor = parsers.soy.parseSync(base, module, file);
            var soyDependencies = _.uniq(_.map(descriptor.soyDependencies, function (dep) {
                var components = dep.split(".");
                components.pop();
                return components.join(".");
            }));
            return {
                v: {
                    namespace: descriptor.soyNamespace,
                    name: fsPath.join(module, descriptor.baseName),
                    dependencies: _.filter(soyDependencies, function (dep) {
                        return dep !== descriptor.soyNamespace;
                    })
                }
            };
        }();

        if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
    } catch (e) {
        console.error(e);
    }
}
function handleFile(dir, fileName) {
    if (parsers.soy.isModulePath(fileName)) {
        // Extracts the relative path and file name from the
        // given path. Expecting it to be in this format:
        // {absolute path}{input directory}/{soy module}/{soy file name}
        var re = new RegExp(".*" + dir + "/(.*)/([^/]*.soy)$");
        var match = re.exec(fileName);
        if (match) {
            var _module = match[1];
            var file = match[2];
            return parseSoyFile(dir, _module, file);
        }
    }
}
function toAMD(descriptor) {
    var dependencyStrings = _.reduce(descriptor.dependencies, function (str, dependencyNS) {
        var dependency = soyDependencyMap[dependencyNS];
        if (!dependency) {
            return str;
        }
        if (str) {
            str += ",";
        }
        return str + "\"" + dependency.name + ".soy\"";
    }, "");
    return "define([" + dependencyStrings + "], function(){ return " + descriptor.namespace + "; });\n";
}
function getModifiedDateForFile(path) {
    if (fs.existsSync(path)) {
        return fs.statSync(path).mtime;
    }
    return -1;
}
function buildSoyDeps(_ref2) {
    var inputDir = _ref2.inputDir,
        jar = _ref2.jar,
        functions = _ref2.functions;


    inputDir.forEach(function (dir) {
        compileSoy({ inputDir: inputDir, jar: jar, functions: functions }, dir);
        var soyFiles = finder.from(dir).findFiles("*.soy$");
        soyFiles.forEach(function (file) {
            var descriptor = handleFile(dir, file);
            soyDependencyMap[descriptor.namespace] = descriptor;
        });
    });
}
module.exports = function () {
    this.cacheable();
    var options = _.merge(this.options.soyLoader, {
        inputDir: this.options.resolve.root,
        cwd: this.options.context
    });
    options.jar = options.jar || BUILT_IN_JAR;
    var callback = this.async();
    var soyFile = this.resourcePath;
    var inputDir = _.find(options.inputDir, function (dir) {
        return soyFile.indexOf(dir) !== -1;
    });
    var descriptor = handleFile(this.options.context, soyFile);
    // Create a temporary directory to store the compiled soy files in, since the atlassian soy cli compiler
    // needs to output a file.
    if (!tempDirectory) {
        tempDirectory = temp.mkdirSync("compiled-soy");
    }
    // Only build the dependency map once
    // We may need to start updating this once webpack is working on a running server and we only change
    // one file at a time - not sure if this map will persist (in which case we need to update it with each change)
    if (_.isEmpty(soyDependencyMap)) {
        buildSoyDeps(options);
    }
    var relativeSoyPath = soyFile.split(inputDir)[1].replace(/^[\\\/]/, "");
    var soyJsFile = tempDirectory + "/" + relativeSoyPath.replace(/\.soy/, ".js");
    var soyModifiedDate = getModifiedDateForFile(soyFile);
    var soyJsModifiedDate = getModifiedDateForFile(soyJsFile);
    // We do some optimising here to only compile soy if it has changed since it last was compiled.
    if (soyModifiedDate > soyJsModifiedDate) {
        compileSoy(options, inputDir, relativeSoyPath);
    }
    fs.readFile(soyJsFile, "utf8", function (err, data) {
        if (err) {
            return console.log(err);
        }
        // Webpack does not like implicit globals, which soy outputs. So we need to link them to the global scope (window).
        if (options.namespaces) {
            options.namespaces.forEach(function (namespace) {
                data = data.replace("var " + namespace, "window." + namespace);
            });
        }
        var amdCode = toAMD(descriptor);
        callback(null, data + amdCode);
    });
};