// Webpack loader that
//   * converts soy files to js
//   * scans for its dependencies on other soy files
//   * appends an AMD define() to the end of the file which includes it's dependencies and returns to the template,
//     which is a JS global.
'use strict';
const parsers = require("atlassian-wrm").parsers;
const _ = require("lodash");
const fsPath = require("path");
const finder = require("fs-finder");
const childProcess = require("child_process");
const fs = require("fs");
const temp = require("temp").track();
const soyDependencyMap = {};
const BUILT_IN_JAR = `${__dirname}/atlassian-soy-cli-support-4.4.1-jar-with-dependencies.jar`;
let tempDirectory;

function formatOptsForExec(obj) {
    return _.values(_.mapValues(obj, (value, name) => {
        const val = _.isArray(value) ? value.split(" ") : value;
        return `--${name} ${val}`;
    })).join(" ");
}

function compileSoy({cwd, jar, functions}, dir, file) {
    const options = {
        type: "js",
        glob: file ? file : "**/*.soy",
        basedir: dir,
        outdir: tempDirectory,
        functions: functions
    };
    const execParams = formatOptsForExec(options);
    const command = `java -Dorg.slf4j.simpleLogger.defaultLogLevel=WARN -jar ${jar} ${execParams} --use-ajs-context-path --use-ajs-i18n`;
    childProcess.execSync(command, {cwd});
}

function parseSoyFile(base, module, file) {
    try {
        const descriptor = parsers.soy.parseSync(base, module, file);
        const soyDependencies = _.uniq(_.map(descriptor.soyDependencies, (dep) => {
            const components = dep.split(".");
            components.pop();
            return components.join(".");
        }));
        return {
            namespace: descriptor.soyNamespace,
            name: fsPath.join(module, descriptor.baseName),
            dependencies: _.filter(soyDependencies, (dep) => {
                return dep !== descriptor.soyNamespace;
            })
        };
    } catch (e) {
        console.error(e);
    }
}
function handleFile(dir, fileName) {
    if (parsers.soy.isModulePath(fileName)) {
        // Extracts the relative path and file name from the
        // given path. Expecting it to be in this format:
        // {absolute path}{input directory}/{soy module}/{soy file name}
        const re = new RegExp(`.*${dir}/(.*)\/([^\/]*\.soy)$`);
        const match = re.exec(fileName);
        if (match) {
            const module = match[1];
            const file = match[2];
            return parseSoyFile(dir, module, file);
        }
    }
}
function toAMD(descriptor) {
    const dependencyStrings = _.reduce(descriptor.dependencies, (str, dependencyNS) => {
        const dependency = soyDependencyMap[dependencyNS];
        if (!dependency) {
            return str;
        }
        if (str) {
            str += ",";
        }
        return `${str}"${dependency.name}.soy"`;
    }, "");
    return `define([${dependencyStrings}], function(){ return ${descriptor.namespace}; });\n`;
}
function getModifiedDateForFile(path) {
    if (fs.existsSync(path)) {
        return fs.statSync(path).mtime;
    }
    return -1;
}
function buildSoyDeps({inputDir, jar, functions}) {

    inputDir.forEach((dir) => {
        compileSoy({inputDir, jar, functions}, dir);
        const soyFiles = finder.from(dir).findFiles("*.soy$");
        soyFiles.forEach((file) => {
            const descriptor = handleFile(dir, file);
            soyDependencyMap[descriptor.namespace] = descriptor;
        });
    });
}
module.exports = function() {
    this.cacheable();
    const options = _.merge(this.options.soyLoader, {
        inputDir: this.options.resolve.root,
        cwd: this.options.context
    });
    options.jar = options.jar || BUILT_IN_JAR;
    const callback = this.async();
    const soyFile = this.resourcePath;
    const inputDir = _.find(options.inputDir, (dir) => {
        return soyFile.indexOf(dir) !== -1;
    });
    const descriptor = handleFile(this.options.context, soyFile);
    // Create a temporary directory to store the compiled soy files in, since the atlassian soy cli compiler
    // needs to output a file.
    if (!tempDirectory) {
        tempDirectory = temp.mkdirSync("compiled-soy");
    }
    // Only build the dependency map once
    // We may need to start updating this once webpack is working on a running server and we only change
    // one file at a time - not sure if this map will persist (in which case we need to update it with each change)
    if (_.isEmpty(soyDependencyMap)) {
        buildSoyDeps(options);
    }
    const relativeSoyPath = soyFile.split(inputDir)[1].replace(/^[\\\/]/, "");
    const soyJsFile = `${tempDirectory}/${relativeSoyPath.replace(/\.soy/, ".js")}`;
    const soyModifiedDate = getModifiedDateForFile(soyFile);
    const soyJsModifiedDate = getModifiedDateForFile(soyJsFile);
    // We do some optimising here to only compile soy if it has changed since it last was compiled.
    if (soyModifiedDate > soyJsModifiedDate) {
        compileSoy(options, inputDir, relativeSoyPath);
    }
    fs.readFile(soyJsFile, "utf8", (err, data) => {
        if (err) {
            return console.log(err);
        }
        // Webpack does not like implicit globals, which soy outputs. So we need to link them to the global scope (window).
        if(options.namespaces) {
            options.namespaces.forEach((namespace) => {
                data = data.replace(`var ${namespace}`, `window.${namespace}`);
            });
        }
        const amdCode = toAMD(descriptor);
        callback(null, data + amdCode);
    });
};